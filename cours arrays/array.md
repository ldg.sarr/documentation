# COURS ARRAY JS  
1. Qu'est-ce que le array
2. Pourquoi utiliser le array ?  
3. Les normes coding

## Qu'est-ce que le array
En JavaScript, array est une seule variable utilisée pour stocker différents éléments. 
## Pourquoi utiliser le array ?  
Array est souventest utilisé lorsque nous voulons stocker une liste d'éléments et y accéder par une seule variable.  
Un tableau en JavaScript peut contenir différents éléments.
Nous pouvons stocker des nombres, des chaînes et des booléens dans un seul tableau.

### Exemple:

`filter_none
luminosité_4
// Storing number, boolean, strings in an Array 
vconst  tab = ["1", 25, "false", {first name: 'landing', ages '9'}  , "Rent", true];`


### Accès aux éléments de tableau
Les tableaux en JavaScript sont indexés à partir de 0 afin que nous puissions accéder aux éléments de tableau 
**propriété Length d'un tableau** 
renvoie la longueur d'un tableau. La longueur d'un tableau est toujours supérieure à l'indice le plus élevé d'un tableau.
l'operateur **forEach**
Permet de recuperer un élément pour chaque action  
a la difference de l'operateur **map**   
Permet de parcourir un tableau et modifier ses éléments 
L'operateur **filter**
Comme son nom l'indique qui permet de filtrer
L'operateur **sort** permet de reorganiser un tableau
L'operateur **evry** permet de verifier si quelque chose dans un tableau repond a une condition
L'operateur **some** c'est l'inverse de **evry** si aucun ni repond 
L'operateur **push** permet d'ajouter un élément a la fin de la liste d'un tableau