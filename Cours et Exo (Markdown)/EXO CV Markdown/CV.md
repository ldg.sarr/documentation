# LANDING SARR  
## CONTACT  
Email: ldg.sarr@gmail.com  
Num: 07 51 86 32 85
Adresse 3 rue Jacques Duclos, chez kouyate 78280
Guyancourt  

### ETUDIANT DEVELOPPEUR WEB JAVASCRIPT  
## COMPETENCES    
**GENERALES**  
- CMS WORDPRESS 
- JavaScript   
- Html - Css   
- Responsive   
- Suite Adobe  
- Suite Office  
- Sublime Text  
    
**LANGUES:**  
- Français   
- Anglais

## EXPERIENCES
### Sept. 2018 - Juin 2019
Développeur web
Openclassrooms: développeur web junior   Paris  
**Missions** :
Intégration d'une maquette  
Personnalisation d'un théme wordpress  
Conception d'une carte interactive de location de vélos    
### Févr. 2018 - Avr. 2018
Assistant stagiaire R.H  
AXA ASSURANCE   Dakar  
Département :  
Direction des ressources humaines  
**Missions** :  
Saisir le bilan social de l'entreprise  
Trie et classement des dossiers du personnel Trie et saisie des demandes
d'emploi et de stage  
## FORMATIONS  
### Sept. 2019 
En cours de formation 
développeur web javascript   
Chez White rabbit école numerique  
### Juin. 2019 
Développeur web junior chez Openclassrooms  
### Oct. 2017
Licence professionnelle en G.R.H
à ISEG
Gestion adminitrative du personnel
### Juil. 2013
Baccalauréat
CPMC
Série littéraire

CENTRES D'INTÉRÊT
Nouvelles Technologies, Programmation, Roller,
basket ball
