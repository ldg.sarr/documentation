 # Cours de Markdown

1. Qu'est-ce que le Markdown?
2. Pourquoi utiliser le Markdown?
3. Outils pour le Markdown?
4. La syntaxe du Markdown

 ## Qu'est-ce que le Markdown ?  


 *Le Markdown est un langage de balisage léger avec une syntaxe de formatage de texte brut conçu pour être converti en HTML ainsi qu'en d'autres format en utilisant un outil du même nom. Le Markdown est souvent utilisé pour rédiger des fichiers readme, pour écrire des messages dans des forums de discussion en ligne et pour créer des textes riches en utilisant un éditeur de texte brut.*  
 ## Pourquoi utiliser le Markdown ?
 Parce que c'est :
  * **FACILE** : La syntaxe est tellement simple que vous pouvez apprendre en une minute ou deux, puis écrire sans trouver quoi que ce soit bizarre ou geek.
  * **RAPIDE** : Cela fait gagner du temps comparé aux autres types de fichiers/formats texte. Cela aide à accélérer la productivité et le flux de travail du rédacteur.
  * **PROPRE** : La syntaxe et le rendu sont tous deux clairs, ne font pas mal aux yeux et sont simples à gérer.
  * **FLEXIBLE** : Avec un peu de configuration, votre texte sera traduit sur toutes les plateformes, sera modifiable dans tout logiciel d'édition et convertible dans un large choix de formats.
 ## La syntaxe du Markdown 

## markdown syntaxes 
### mot en italique
Voici un mot *en italique* 
Votre mot se trouve entre astérisques `*mon-mot*`
-----------------
### un mot en gras
Voici un mot __en gras__ ! 
Votre mot se trouve entre deux `__underscores__` 
-----------------
### Les titres
# Titre de niveau 1 
Pour un titre de niveau 1 (h1), il faut placer un `#titre` devant votre titre.
## Titre de niveau 2
Pour un titre de niveau 2 (h2), il faut cette fois deux `##titre` devant votre titre
Et ainsi de suite jusqu'au h6.

### Aller à la ligne en fin de phrase

Voici un  
Pour faire un  
changement de ligne

Votre ligne doit se terminer par 2 `espaces` pour faire ce qu'on appelle un __retour-chariot__, c'est à dire aller à la ligne.

`Voici un  `
`changement de ligne`

### Faire une liste à puces
* Une puce
* Une autre puce
* Et encore une autre puce !
Il faut simplement placer un astérisque devant les éléments de votre liste.
`* Une puce`
`* Une autre puce`
`* Et encore une autre puce !`
### Pour faire une liste ordonnée : 
1. Et de un
2. Et de deux